unit SendUserInfo;

interface
 uses
  System.SysUtils,
  System.Classes,
  System.Win.Registry,
  Winapi.Windows,
  Winapi.Messages,
  System.Variants
  , System.IOUtils
  , System.IniFiles
  , WinAPI.SHFolder
  , Winsock
  , Vcl.Forms
//  , System.Hash
  ;
procedure sendinfo(apath:string);

var
   cProjectID,cPublicKey,cSecretKEY : string;

implementation
 uses
   RavenMessagesToSentry;

 //��������� �������

function ReadEnigmaKeyRegistry : string;
var rr : Tregistry;
begin
  rr := TRegistry.Create;
  rr.RootKey := HKEY_CURRENT_USER;

  if rr.KeyExists('Software\Enigma Protector\')
  then
    begin
     Result :=  Rr.ReadString('Enigma Protector')
    end
  else
     Result := '����������� ����������' ;
end;


function LocalHostIP: string;         // ��� ������
var HostEnt: PHostEnt;
    Buffer: array [0..63] of AnsiChar;
    GInitData: TWSAData;
begin
   Result:='';
   WSAStartup($101, GInitData);
   GetHostName(Buffer, SizeOf(Buffer));
   HostEnt:=GetHostByName(Buffer);
   if (HostEnt<>nil) then
   begin
      while (PInAddr(HostEnt^.h_addr_list^)<>nil) do
      begin
         if (Result<>'') then
            Result:=Result+';';
         Result:=Result+inet_ntoa(PInAddr(HostEnt^.h_addr_list^)^);
         Inc(HostEnt^.h_addr_list);
      end;
   end;
   WSACleanup;
end;

function GetIP:string;  // ������ ���� ����� �� �����
const WSVer = $101;
var wsaData: TWSAData;
    P: PHostEnt;
    Buf: array [0..127] of Char;
begin
     Result := '';
     if (WSAStartup(WSVer, wsaData) = 0) then
     begin
          if (GetHostName(@Buf, 128) = 0) then
          begin
               P := GetHostByName(@Buf);
               if (P <> nil) then
                   Result := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
          end;
          WSACleanup;
     end;
end;

function GetAppVersion: string;
var
  n, Len: DWORD;
  Buf: PChar;
  Value: PChar;
begin
  n := GetFileVersionInfoSize(PChar(Application.ExeName), n);
  if n > 0 then
  begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(Application.ExeName), 0, n, Buf);
    if VerQueryValue(Buf, PChar('StringFileInfo\041904E3\FileVersion'),
      Pointer(Value), Len) then
      result := Value;
    FreeMem(Buf, n);
  end
  else
    result := '';
end;

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function GetSystemUserName: string;
var // �������� ��� ������������ ������
  UserName: array[0..255] of Char;
  UserNameSize: DWORD;
begin
  UserNameSize := 255;
  if GetUserName(@UserName, UserNameSize) then
    Result := string(UserName)
  else
    Result := '';
end;


function ReadEnigmaKeyFile(apath:string) : string;
var rr : TINIFile;
   // auser: string;
   // LStr: array[0 .. MAX_PATH] of Char;

begin
 // auser := GetSystemUserName;
 // aPath := aPath.Join('\',['C:\users',auser,'AppData\roaming\itt','license.dat']);

 { if SHGetFolderPath(0, CSIDL_APPDATA, 0, 0, @LStr) = S_OK
  then
    begin
     auser :=  LStr;
     aPath := aPath.Join('\',[auser,'itt','license.dat'])
    end
  else
    aPath := aPath.Join('\',['C:\users',auser,'AppData\roaming\itt','license.dat']);
  }
  Result := '����������� ����������';

  try
    rr := TINIFile.Create(aPath);
    if rr.SectionExists('Registration information') then
      begin
       Result :=  Rr.ReadString('Registration information', 'Name', '����������� ����������')
      end
    else
       Result := '����������� ����������' ;
  finally
    rr.Free;
  end;
end;

procedure sendinfo(apath:string);
var aparam : Tarray<string>;
    astr : string;
    adm : TdmRavenToSentry;
    _Thread1: TThread;
begin
 //�������� ���������� raven
 //����� ���������� �������
  SetLength(aparam,0);
  astr := 'app_name:' + ExtractFileName(Application.ExeName);
  aparam :=aparam +[astr];
  astr := 'app_version:' + GetAppVersion;
  aparam :=aparam +[astr];
  astr := 'comp_ip:' + LocalHostIP;
  aparam :=aparam +[astr];
  astr := 'comp_name:' + GetComputerNetName;
  aparam :=aparam +[astr];
  astr := 'user_name:' + GetSystemUserName;
  aparam :=aparam +[astr];
  astr := 'registry_user:' + ReadEnigmaKeyFile(apath);
  aparam :=aparam +[astr];

  _Thread1 := TThread.CreateAnonymousThread(procedure
    begin
     adm := TdmRavenToSentry.Create(nil);
     adm.conSentry.ProjectID := cProjectID;
     adm.conSentry.PublicKey := cPublicKey;
     adm.conSentry.SecretKey := cSecretKEY;
     adm.rvnclnt1.sendMessage('���������� � ���������� ����������', aparam);
     //�������� �������
     adm.Free;
    end);
  _Thread1.Start;
end;

// initialization
//sendinf0
end.
